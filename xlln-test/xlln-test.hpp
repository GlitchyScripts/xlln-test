#pragma once
#include <stdint.h>
#include <stdio.h>
#include "resource.h"
#include "xlive/xdefs.hpp"

#ifdef _DEBUG
// Move this out to enable building with debug logs in release mode.
#define XLLN_DEBUG
#endif

#define XLLN_LOG_LEVEL_MASK		0b00111111
// Function call tracing.
#define XLLN_LOG_LEVEL_TRACE	0b00000001
// Function, variable and operation logging.
#define XLLN_LOG_LEVEL_DEBUG	0b00000010
// Generally useful information to log (service start/stop, configuration assumptions, etc).
#define XLLN_LOG_LEVEL_INFO		0b00000100
// Anything that can potentially cause application oddities, but is being handled adequately.
#define XLLN_LOG_LEVEL_WARN		0b00001000
// Any error which is fatal to the operation, but not the service or application (can't open a required file, missing data, etc.).
#define XLLN_LOG_LEVEL_ERROR	0b00010000
// Errors that will terminate the application.
#define XLLN_LOG_LEVEL_FATAL	0b00100000

#define XLLN_LOG_CONTEXT_MASK			(0b11000111 << 8)
// Logs related to Xlive(GFWL) functionality.
#define XLLN_LOG_CONTEXT_XLIVE			(0b00000001 << 8)
// Logs related to XLiveLessNess functionality.
#define XLLN_LOG_CONTEXT_XLIVELESSNESS	(0b00000010 << 8)
// Logs related to XLLN-Module functionality.
#define XLLN_LOG_CONTEXT_XLLN_MODULE	(0b00000100 << 8)
// Logs related to functionality from other areas of the application.
#define XLLN_LOG_CONTEXT_OTHER			(0b10000000 << 8)
// Logs related to the Title functionality.
#define XLLN_LOG_CONTEXT_TITLE			(0b01000000 << 8)

// #41144
uint32_t WINAPI XLLNDebugLogF(uint32_t log_level, const char* const message_format, ...);

extern bool xlln_is_XLLNDebugLogF_available;

#ifdef XLLN_DEBUG
#define XLLN_PRINT_LOG(log_level, message_format, ...) {if (xlln_is_XLLNDebugLogF_available) { XLLNDebugLogF(log_level, message_format, __VA_ARGS__); } else { printf(message_format, __VA_ARGS__); putchar('\r'); putchar('\n'); }}
#define XLLN_PRINT_LOG_ECODE(error_code, log_level, message_format, ...) {if (xlln_is_XLLNDebugLogF_available) { XLLNDebugLogF(log_level, message_format, __VA_ARGS__); XLLNDebugLogF(log_level, " Error code: 0x%08x.", error_code); } else { printf(message_format, __VA_ARGS__); printf(" Error code: 0x%08x.", error_code); putchar('\r'); putchar('\n'); }}
#else
#define XLLN_PRINT_LOG(log_level, message_format, ...)
#define XLLN_PRINT_LOG_ECODE(error_code, log_level, message_format, ...)
#endif

#define XLIVE_IMPORT_EXISTS(ordinal) GetProcAddress(xlln_hmod_xlive, (PCSTR)(ordinal))

extern HMODULE xlln_hmod_xlive;

// #41140
uint32_t WINAPI XLLNLogin(uint32_t user_index, BOOL lsb_8_live_enabled_8_online_disabled_msb_16_reserved, uint32_t user_id, const char* user_username);
// #41141
uint32_t WINAPI XLLNLogout(uint32_t user_index);
