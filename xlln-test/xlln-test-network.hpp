#pragma once
#include <stdint.h>

namespace XllnTestPacketType {
	const char* const TYPE_NAMES[] {
		"UNKNOWN",
		"BROADCAST",
	};
	typedef enum : uint8_t {
		UNKNOWN = 0,
		FIRST,
		BROADCAST = FIRST,
		LAST = BROADCAST,
	} TYPE;
	
	const char* GetPacketTypeName(XllnTestPacketType::TYPE type);

#pragma pack(push, 1) // Save then set byte alignment setting.

	typedef struct _PACKET_BROADCAST {
		uint32_t idk = 0;
	} PACKET_BROADCAST;

#pragma pack(pop) // Return to original alignment setting.

}

bool XllnTestNetwork(bool use_xlive_networking);
