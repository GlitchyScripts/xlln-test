#include <WinSock2.h>
#include <windows.h>
#include "xlln-test-network.hpp"
#include "xlln-test.hpp"
#include "utils/utils.hpp"
#include "xlive/xfuncs.hpp"

const uint16_t xlln_test_socket_main_port = 1240;

//XWSASend;
//XWSASendTo;
//XWSARecv;
//XWSARecvFrom;
//XWSACancelOverlappedIO;
//XWSAGetOverlappedResult;
//XWSACreateEvent;
//XWSACloseEvent;
//XWSAFDIsSet;
//XWSASetEvent;
//XWSAResetEvent;
//XWSAEventSelect;
//XWSAWaitForMultipleEvents;
//XWSASetLastError;
//WSAGetLastError;

const char* XllnTestPacketType::GetPacketTypeName(XllnTestPacketType::TYPE type)
{
	if (type >= XllnTestPacketType::TYPE::FIRST && type <= XllnTestPacketType::TYPE::LAST) {
		return TYPE_NAMES[type];
	}
	
	return TYPE_NAMES[XllnTestPacketType::TYPE::UNKNOWN];
}

static bool ParseUserCommands(SOCKET socket_main, bool use_xlive_networking, bool blocking_networking_mode, bool use_wsa_functions)
{
	WSAEVENT wsaEvent = WSA_INVALID_EVENT;
	if (use_xlive_networking) {
		wsaEvent = XWSACreateEvent();
	}
	else {
		wsaEvent = WSACreateEvent();
	}
	if (wsaEvent == WSA_INVALID_EVENT) {
		int32_t errorWsaCreateEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCreateEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACreateEvent failed."
			, __func__
		);
		return false;
	}
	
	bool resultSuccess = true;
	
	WSAOVERLAPPED wsaOverlapped;
	memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
	
	wchar_t userAction[100];
	XOVERLAPPED overlapped;
	while (1) {
		userAction[0] = 0;
		memset(&overlapped, 0, sizeof(overlapped));
		overlapped.InternalLow = ERROR_IO_PENDING;
		uint32_t resultXShowKeyboardUI = XShowKeyboardUI(
			0, 0
			, L""
			, L"XLLN Test"
			, L"Submit command:"
			, userAction, sizeof(userAction)/sizeof(userAction[0])
			, &overlapped
		);
		if (resultXShowKeyboardUI != ERROR_IO_PENDING) {
			XLLN_PRINT_LOG_ECODE(resultXShowKeyboardUI, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XShowKeyboardUI failed."
				, __func__
			);
			resultSuccess = false;
			break;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s waiting for next command."
			, __func__
		);
		
		while (overlapped.InternalLow == ERROR_IO_PENDING) {
			Sleep(100);
		}
		
		size_t extendedError = XGetOverlappedExtendedError(&overlapped);
		if (extendedError == ERROR_CANCELLED) {
			break;
		}
		
		if (userAction[0] == 0) {
			continue;
		}
		
		if (_wcsicmp(userAction, L"exit") == 0 || _wcsicmp(userAction, L"quit") == 0) {
			break;
		}
		
		if (_wcsicmp(userAction, L"broadcast") == 0) {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s broadcasting."
				, __func__
			);
			
			const int32_t packetSizeType = (int32_t)sizeof(XllnTestPacketType::TYPE);
			const int32_t packetSizeTypeBroadcast = (int32_t)sizeof(XllnTestPacketType::PACKET_BROADCAST);
			const int32_t packetSizeTotal = packetSizeType + packetSizeTypeBroadcast;
			
			uint8_t sendData[packetSizeTotal];
			int32_t iSendData = 0;
			
			XllnTestPacketType::TYPE &packetType = *(XllnTestPacketType::TYPE*)&sendData[iSendData];
			iSendData += packetSizeType;
			packetType = XllnTestPacketType::TYPE::BROADCAST;
			
			XllnTestPacketType::PACKET_BROADCAST &packetBroadcast = *(XllnTestPacketType::PACKET_BROADCAST*)&sendData[iSendData];
			iSendData += packetSizeTypeBroadcast;
			packetBroadcast.idk = 0;
			
			SOCKADDR_IN sendToAddr;
			sendToAddr.sin_family = AF_INET;
			sendToAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
			//sendToAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
			sendToAddr.sin_port = htons(xlln_test_socket_main_port);
			
			if (blocking_networking_mode) {
				if (use_wsa_functions) {
					uint32_t dataSentSize = 0;
					
					WSABUF* wsaBuf = new WSABUF;
					wsaBuf->len = packetSizeTotal;
					wsaBuf->buf = (char*)sendData;
					int32_t resultXWsaSendTo = SOCKET_ERROR;
					if (use_xlive_networking) {
						resultXWsaSendTo = XWSASendTo(socket_main, wsaBuf, 1, &dataSentSize, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr), 0, 0);
					}
					else {
						resultXWsaSendTo = WSASendTo(socket_main, wsaBuf, 1, (DWORD*)&dataSentSize, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr), 0, 0);
					}
					wsaBuf->len = 0;
					wsaBuf->buf = 0;
					delete wsaBuf;
					wsaBuf = 0;
					if (resultXWsaSendTo || dataSentSize != packetSizeTotal) {
						int32_t errorSendTo = WSAGetLastError();
						XLLN_PRINT_LOG_ECODE(errorSendTo, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
							, "%s \"%ls\" XWSASendTo failed."
							, __func__
							, userAction
						);
					}
				}
				else {
					int32_t dataSentSize = SOCKET_ERROR;
					if (use_xlive_networking) {
						dataSentSize = XSocketSendTo(socket_main, (char*)sendData, packetSizeTotal, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr));
					}
					else {
						dataSentSize = sendto(socket_main, (char*)sendData, packetSizeTotal, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr));
					}
					if (dataSentSize != packetSizeTotal) {
						int32_t errorSendTo = WSAGetLastError();
						XLLN_PRINT_LOG_ECODE(errorSendTo, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
							, "%s \"%ls\" XSocketSendTo failed."
							, __func__
							, userAction
						);
					}
				}
			}
			else {
				uint32_t dataSentSize = 0;
				
				memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
				wsaOverlapped.hEvent = wsaEvent;
				
				WSABUF* wsaBuf = new WSABUF;
				wsaBuf->len = packetSizeTotal;
				wsaBuf->buf = (char*)sendData;
				int32_t resultXWsaSendTo = SOCKET_ERROR;
				if (use_xlive_networking) {
					resultXWsaSendTo = XWSASendTo(socket_main, wsaBuf, 1, 0, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr), &wsaOverlapped, 0);
				}
				else {
					resultXWsaSendTo = WSASendTo(socket_main, wsaBuf, 1, 0, 0, (sockaddr*)&sendToAddr, sizeof(sendToAddr), &wsaOverlapped, 0);
				}
				wsaBuf->len = 0;
				wsaBuf->buf = 0;
				delete wsaBuf;
				wsaBuf = 0;
				
				uint32_t resultXWsaWait = WSA_WAIT_FAILED;
				if (use_xlive_networking) {
					resultXWsaWait = XWSAWaitForMultipleEvents(1, &wsaEvent, TRUE, INFINITE, FALSE);
				}
				else {
					resultXWsaWait = WSAWaitForMultipleEvents(1, &wsaEvent, TRUE, INFINITE, FALSE);
				}
				if (resultXWsaWait != WSA_WAIT_EVENT_0) {
					int32_t errorXWsaWait = WSAGetLastError();
					XLLN_PRINT_LOG_ECODE(errorXWsaWait, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
						, "%s \"%ls\" XWSAWaitForMultipleEvents failed."
						, __func__
						, userAction
					);
					resultSuccess = false;
					break;
				}
				
				uint32_t wsaFlags = 0;
				BOOL resultXWsaGetOverlappedResult = FALSE;
				if (use_xlive_networking) {
					resultXWsaGetOverlappedResult = XWSAGetOverlappedResult(socket_main, &wsaOverlapped, &dataSentSize, FALSE, &wsaFlags);
				}
				else {
					resultXWsaGetOverlappedResult = WSAGetOverlappedResult(socket_main, &wsaOverlapped, (DWORD*)&dataSentSize, FALSE, (DWORD*)&wsaFlags);
				}
				if (!resultXWsaGetOverlappedResult || dataSentSize != packetSizeTotal) {
					int32_t errorXWsaGetOverlappedResult = WSAGetLastError();
					XLLN_PRINT_LOG_ECODE(errorXWsaGetOverlappedResult, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
						, "%s \"%ls\" XWSAGetOverlappedResult failed."
						, __func__
						, userAction
					);
					resultSuccess = false;
					break;
				}
			}
			
			continue;
		}
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s exiting."
		, __func__
	);
	
	BOOL resultXWsaCloseEvent = FALSE;
	if (use_xlive_networking) {
		resultXWsaCloseEvent = XWSACloseEvent(wsaEvent);
	}
	else {
		resultXWsaCloseEvent = WSACloseEvent(wsaEvent);
	}
	if (!resultXWsaCloseEvent) {
		int32_t errorWsaCloseEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCloseEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACloseEvent failed."
			, __func__
		);
		resultSuccess = false;
	}
	wsaEvent = 0;
	
	memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
	memset(&overlapped, 0, sizeof(overlapped));
	
	return resultSuccess;
}

static bool ParseNetworkData(SOCKET perpetual_socket, uint8_t* data_buffer, int32_t data_size, SOCKADDR_STORAGE* sock_addr_external)
{
	const int32_t packetSizeType = (int32_t)sizeof(XllnTestPacketType::TYPE);
	const int32_t packetSizeTypeBroadcast = (int32_t)sizeof(XllnTestPacketType::PACKET_BROADCAST);
	
	if (data_size < packetSizeType) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_WARN
			, "%s data_size (%d) < packetSizeType (%d)."
			, __func__
			, data_size
			, packetSizeType
		);
		return false;
	}
	
	int32_t iData = 0;
	
	XllnTestPacketType::TYPE &packetType = *(XllnTestPacketType::TYPE*)&data_buffer[iData];
	iData += packetSizeType;
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s Received %s packet."
		, __func__
		, XllnTestPacketType::GetPacketTypeName(packetType)
	);
	
	switch (packetType) {
		case XllnTestPacketType::TYPE::BROADCAST: {
			
			return true;
		}
	}
	
	return false;
}

typedef struct _THREAD_PARAMS_SOCKET_MAIN {
	SOCKET networkSocket = INVALID_SOCKET;
	bool useXliveNetworking = true;
	bool blockingNetworkingMode = true;
	bool useWsaFunctions = false;
} THREAD_PARAMS_SOCKET_MAIN;

static DWORD WINAPI ThreadSocketMain(void* lpParam)
{
	THREAD_PARAMS_SOCKET_MAIN* thread_params = (THREAD_PARAMS_SOCKET_MAIN*)lpParam;
	
	SOCKET network_socket = thread_params->networkSocket;
	bool use_xlive_networking = thread_params->useXliveNetworking;
	bool use_wsa_functions = thread_params->useWsaFunctions;
	bool blocking_networking_mode = thread_params->blockingNetworkingMode;
	
	delete thread_params;
	thread_params = 0;
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s starting."
		, __func__
	);
	
	WSAEVENT wsaEvent = WSA_INVALID_EVENT;
	if (use_xlive_networking) {
		wsaEvent = XWSACreateEvent();
	}
	else {
		wsaEvent = WSACreateEvent();
	}
	if (wsaEvent == WSA_INVALID_EVENT) {
		int32_t errorWsaCreateEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCreateEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACreateEvent failed."
			, __func__
		);
		return ERROR_FUNCTION_FAILED;
	}
	
	WSAOVERLAPPED wsaOverlapped;
	memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
	
	const int32_t dataBufferSize = 1024;
	char dataBuffer[dataBufferSize];
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	while (1) {
		uint32_t resultDataSize = 0;
		
		if (blocking_networking_mode) {
			if (!use_wsa_functions) {
				int32_t resultRecvFromDataSize = SOCKET_ERROR;
				if (use_xlive_networking) {
					resultRecvFromDataSize = XSocketRecvFrom(network_socket, dataBuffer, dataBufferSize, 0, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
				}
				else {
					resultRecvFromDataSize = recvfrom(network_socket, dataBuffer, dataBufferSize, 0, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
				}
				if (resultRecvFromDataSize < 0) {
					int32_t resultErrorRecvFrom = WSAGetLastError();
					if (resultErrorRecvFrom == WSAEWOULDBLOCK) {
						// Normal error. No more data.
						break;
					}
					
					XLLN_PRINT_LOG_ECODE(resultErrorRecvFrom, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
						, "%s XSocketRecvFrom failed."
						, __func__
					);
					
					break;
				}
				
				resultDataSize = resultRecvFromDataSize;
			}
			else {
				uint32_t wsaFlags = 0;
				WSABUF* wsaBuf = new WSABUF;
				wsaBuf->len = dataBufferSize;
				wsaBuf->buf = dataBuffer;
				int32_t resultXWsaRecvFrom = SOCKET_ERROR;
				if (use_xlive_networking) {
					resultXWsaRecvFrom = XWSARecvFrom(network_socket, wsaBuf, 1, &resultDataSize, &wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, 0, 0);
				}
				else {
					resultXWsaRecvFrom = WSARecvFrom(network_socket, wsaBuf, 1, (DWORD*)&resultDataSize, (DWORD*)&wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, 0, 0);
				}
				wsaBuf->len = 0;
				wsaBuf->buf = 0;
				delete wsaBuf;
				wsaBuf = 0;
				if (resultXWsaRecvFrom) {
					int32_t resultErrorXWsaRecvFrom = WSAGetLastError();
					XLLN_PRINT_LOG_ECODE(resultErrorXWsaRecvFrom, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
						, "%s XWSARecvFrom failed."
						, __func__
					);
					break;
				}
			}
		}
		else {
			memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
			wsaOverlapped.hEvent = wsaEvent;
			
			uint32_t wsaFlags = 0;
			WSABUF* wsaBuf = new WSABUF;
			wsaBuf->len = dataBufferSize;
			wsaBuf->buf = dataBuffer;
			int32_t resultXWsaRecvFrom = SOCKET_ERROR;
			if (use_xlive_networking) {
				resultXWsaRecvFrom = XWSARecvFrom(network_socket, wsaBuf, 1, 0, &wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, &wsaOverlapped, 0);
			}
			else {
				resultXWsaRecvFrom = WSARecvFrom(network_socket, wsaBuf, 1, 0, (DWORD*)&wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, &wsaOverlapped, 0);
			}
			wsaBuf->len = 0;
			wsaBuf->buf = 0;
			delete wsaBuf;
			wsaBuf = 0;
			
			int32_t resultErrorXWsaRecvFrom = WSAGetLastError();
			if (
				!(resultXWsaRecvFrom == SOCKET_ERROR && resultErrorXWsaRecvFrom == WSA_IO_PENDING)
				&& !(!resultXWsaRecvFrom && !resultErrorXWsaRecvFrom)
			) {
				XLLN_PRINT_LOG_ECODE(resultErrorXWsaRecvFrom, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XWSARecvFrom failed."
					, __func__
				);
				break;
			}
			
			//uint32_t resultXWsaWait = WSA_WAIT_FAILED;
			//if (use_xlive_networking) {
			//	resultXWsaWait = XWSAWaitForMultipleEvents(1, &wsaEvent, TRUE, INFINITE, FALSE);
			//}
			//else {
			//	resultXWsaWait = WSAWaitForMultipleEvents(1, &wsaEvent, TRUE, INFINITE, FALSE);
			//}
			//if (resultXWsaWait != WSA_WAIT_EVENT_0) {
			//	int32_t errorXWsaWait = WSAGetLastError();
			//	XLLN_PRINT_LOG_ECODE(errorXWsaWait, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			//		, "%s XWSAWaitForMultipleEvents failed."
			//		, __func__
			//	);
			//	break;
			//}
			
			wsaFlags = 0;
			BOOL resultXWsaGetOverlappedResult = FALSE;
			if (use_xlive_networking) {
				resultXWsaGetOverlappedResult = XWSAGetOverlappedResult(network_socket, &wsaOverlapped, &resultDataSize, TRUE, &wsaFlags);
			}
			else {
				resultXWsaGetOverlappedResult = WSAGetOverlappedResult(network_socket, &wsaOverlapped, (DWORD*)&resultDataSize, TRUE, (DWORD*)&wsaFlags);
			}
			if (!resultXWsaGetOverlappedResult) {
				int32_t errorXWsaGetOverlappedResult = WSAGetLastError();
				XLLN_PRINT_LOG_ECODE(errorXWsaGetOverlappedResult, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XWSAGetOverlappedResult failed."
					, __func__
				);
				break;
			}
		}
		
		ParseNetworkData(network_socket, (uint8_t*)dataBuffer, resultDataSize, &sockAddrExternal);
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s exiting."
		, __func__
	);
	
	BOOL resultXWsaCloseEvent = FALSE;
	if (use_xlive_networking) {
		resultXWsaCloseEvent = XWSACloseEvent(wsaEvent);
	}
	else {
		resultXWsaCloseEvent = WSACloseEvent(wsaEvent);
	}
	if (!resultXWsaCloseEvent) {
		int32_t errorWsaCloseEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCloseEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACloseEvent failed."
			, __func__
		);
	}
	wsaEvent = 0;
	
	return ERROR_SUCCESS;
}

static bool SocketMainCreate(SOCKET* socket_main, bool use_xlive_networking, bool blocking_mode)
{
	if (use_xlive_networking) {
		*socket_main = XSocketCreate(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	}
	else {
		*socket_main = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	}
	if (*socket_main == INVALID_SOCKET) {
		int32_t errorSocketCreate = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorSocketCreate, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to create."
			, __func__
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s (0x%zx) created."
		, __func__
		, *socket_main
	);
	
	uint32_t sockOptValue = 1;
	int32_t resultSetSockOpt = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultSetSockOpt = XSocketSetSockOpt(*socket_main, SOL_SOCKET, SO_BROADCAST, (const char*)&sockOptValue, sizeof(sockOptValue));
	}
	else {
		resultSetSockOpt = setsockopt(*socket_main, SOL_SOCKET, SO_BROADCAST, (const char*)&sockOptValue, sizeof(sockOptValue));
	}
	if (resultSetSockOpt == SOCKET_ERROR) {
		int32_t errorSetSockOpt = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorSetSockOpt, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to set Broadcast option."
			, __func__
		);
		return false;
	}
	
	// If iMode = 0, blocking is enabled; 
	// If iMode != 0, non-blocking mode is enabled.
	unsigned long iMode = blocking_mode ? 0 : 1;
	int32_t resultIoCtlSocket = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultIoCtlSocket = XSocketIOCTLSocket(*socket_main, FIONBIO, &iMode);
	}
	else {
		resultIoCtlSocket = ioctlsocket(*socket_main, FIONBIO, &iMode);
	}
	if (resultIoCtlSocket == SOCKET_ERROR) {
		int32_t errorIoCtlSocket = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorIoCtlSocket, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to set FIONBIO option."
			, __func__
		);
		return false;
	}
	
	sockaddr_in socketBindAddress;
	socketBindAddress.sin_family = AF_INET;
	socketBindAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	socketBindAddress.sin_port = htons(xlln_test_socket_main_port);
	SOCKET resultSocketBind = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultSocketBind = XSocketBind(*socket_main, (sockaddr*)&socketBindAddress, sizeof(socketBindAddress));
	}
	else {
		resultSocketBind = bind(*socket_main, (sockaddr*)&socketBindAddress, sizeof(socketBindAddress));
	}
	if (resultSocketBind) {
		int32_t errorSocketBind = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorSocketBind, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to bind to port %hu."
			, __func__
			, ntohs(socketBindAddress.sin_port)
		);
		return false;
	}
	
	return true;
}

static bool SocketMainClose(SOCKET* socket_main, bool use_xlive_networking)
{
	if (*socket_main == INVALID_SOCKET) {
		return true;
	}
	
	bool result = true;
	
	int32_t resultSocketShutdown = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultSocketShutdown = XSocketShutdown(*socket_main, SD_RECEIVE);
	}
	else {
		resultSocketShutdown = shutdown(*socket_main, SD_RECEIVE);
	}
	if (resultSocketShutdown) {
		int32_t errorSocketShutdown = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorSocketShutdown, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to shutdown."
			, __func__
		);
		result = false;
	}
	
	int32_t resultSocketClose = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultSocketClose = XSocketClose(*socket_main);
	}
	else {
		resultSocketClose = closesocket(*socket_main);
	}
	if (resultSocketClose) {
		int32_t errorSocketClose = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorSocketClose, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to close."
			, __func__
		);
		result = false;
	}
	
	if (result) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s (0x%zx) closed."
			, __func__
			, *socket_main
		);
	}
	
	*socket_main = INVALID_SOCKET;
	
	return result;
}

bool XllnTestNetwork(bool use_xlive_networking)
{
	bool blockingNetworkingMode = true;
	bool useWsaFunctions = false;
	
	{
		const wchar_t* userActions[] = {L"Classic", L"WSA Blocking", L"WSA Async"};
		XOVERLAPPED overlapped;
		memset(&overlapped, 0, sizeof(overlapped));
		overlapped.InternalLow = ERROR_IO_PENDING;
		MESSAGEBOX_RESULT resultMsgBox;
		uint32_t resultXShowMessageBoxUI = XShowMessageBoxUI(
			0
			, L"XLLN Test"
			, L"Choose networking model:"
			, sizeof(userActions)/sizeof(userActions[0])
			, userActions
			, 0
			, 0
			, &resultMsgBox
			, &overlapped
		);
		if (resultXShowMessageBoxUI != ERROR_IO_PENDING) {
			XLLN_PRINT_LOG_ECODE(resultXShowMessageBoxUI, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XShowMessageBoxUI failed."
				, __func__
			);
			return false;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s Asking for networking model."
			, __func__
		);
		
		while (overlapped.InternalLow == ERROR_IO_PENDING) {
			Sleep(100);
		}
		
		size_t extendedError = XGetOverlappedExtendedError(&overlapped);
		if (extendedError == ERROR_CANCELLED) {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s User cancelled."
				, __func__
			);
			return false;
		}
		
		switch (resultMsgBox.dwButtonPressed) {
			case 0: {
				blockingNetworkingMode = true;
				useWsaFunctions = false;
				break;
			}
			case 1: {
				blockingNetworkingMode = true;
				useWsaFunctions = true;
				break;
			}
			case 2: {
				blockingNetworkingMode = false;
				useWsaFunctions = true;
				break;
			}
			default: {
				XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XShowMessageBoxUI unknown button pressed (0x%x)."
					, __func__
					, resultMsgBox.dwButtonPressed
				);
				return false;
			}
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s User chose %ls."
			, __func__
			, userActions[resultMsgBox.dwButtonPressed]
		);
	}
	
	if (!blockingNetworkingMode && !useWsaFunctions) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s This test program does not support non-blocking non-WSA networking."
			, __func__
		);
		return false;
	}
	
	SOCKET socketMain = INVALID_SOCKET;
	bool resultSocketMain = SocketMainCreate(&socketMain, use_xlive_networking, blockingNetworkingMode);
	if (!resultSocketMain) {
		resultSocketMain = SocketMainClose(&socketMain, use_xlive_networking);
		return false;
	}
	
	{
		THREAD_PARAMS_SOCKET_MAIN* threadParams = new THREAD_PARAMS_SOCKET_MAIN;
		threadParams->networkSocket = socketMain;
		threadParams->useXliveNetworking = use_xlive_networking;
		threadParams->blockingNetworkingMode = blockingNetworkingMode;
		threadParams->useWsaFunctions = useWsaFunctions;
		
		HANDLE resultCreateThread = CreateThread(0, 0, ThreadSocketMain, (void*)threadParams, 0, 0);
		if (!resultCreateThread) {
			uint32_t errorSocketShutdown = GetLastError();
			XLLN_PRINT_LOG_ECODE(errorSocketShutdown, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s Failed to create ThreadSocketMain."
				, __func__
			);
			delete threadParams;
			threadParams = 0;
			resultSocketMain = SocketMainClose(&socketMain, use_xlive_networking);
			return false;
		}
	}
	
	bool resultParseUserCommands = ParseUserCommands(socketMain, use_xlive_networking, blockingNetworkingMode, useWsaFunctions);
	
	resultSocketMain = SocketMainClose(&socketMain, use_xlive_networking);
	
	return resultParseUserCommands && resultSocketMain;
}

bool XllnTestNetwork2(bool use_xlive_networking)
{
	SOCKET socketMain = INVALID_SOCKET;
	bool resultSocketMain = SocketMainCreate(&socketMain, use_xlive_networking, true);
	if (!resultSocketMain) {
		resultSocketMain = SocketMainClose(&socketMain, use_xlive_networking);
		return false;
	}
	
	WSAEVENT wsaEvent = WSA_INVALID_EVENT;
	if (use_xlive_networking) {
		wsaEvent = XWSACreateEvent();
	}
	else {
		wsaEvent = WSACreateEvent();
	}
	if (wsaEvent == WSA_INVALID_EVENT) {
		int32_t errorWsaCreateEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCreateEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACreateEvent failed."
			, __func__
		);
		resultSocketMain = SocketMainClose(&socketMain, use_xlive_networking);
		return false;
	}
	
	const int32_t dataBufferSize = 1024;
	char dataBuffer[dataBufferSize];
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	
	WSAOVERLAPPED wsaOverlapped;
	memset(&wsaOverlapped, 0, sizeof(wsaOverlapped));
	//wsaOverlapped.hEvent = INVALID_HANDLE_VALUE;
	//wsaOverlapped.hEvent = 0;
	wsaOverlapped.hEvent = wsaEvent;
	
	uint32_t wsaFlags = 0;
	WSABUF* wsaBuf = new WSABUF;
	wsaBuf->len = dataBufferSize;
	wsaBuf->buf = dataBuffer;
	int32_t resultXWsaRecvFrom = SOCKET_ERROR;
	if (use_xlive_networking) {
		resultXWsaRecvFrom = XWSARecvFrom(socketMain, wsaBuf, 1, 0, &wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, &wsaOverlapped, 0);
	}
	else {
		resultXWsaRecvFrom = WSARecvFrom(socketMain, wsaBuf, 1, 0, (DWORD*)&wsaFlags, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize, &wsaOverlapped, 0);
	}
	wsaBuf->len = 0;
	wsaBuf->buf = 0;
	delete wsaBuf;
	wsaBuf = 0;
	
	bool resultSuccess = true;
	
	int32_t resultErrorXWsaRecvFrom = WSAGetLastError();
	if (
		!(resultXWsaRecvFrom == SOCKET_ERROR && resultErrorXWsaRecvFrom == WSA_IO_PENDING)
		&& !(!resultXWsaRecvFrom && !resultErrorXWsaRecvFrom)
	) {
		XLLN_PRINT_LOG_ECODE(resultErrorXWsaRecvFrom, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSARecvFrom failed."
			, __func__
		);
		resultSuccess = false;
	}
	
	uint32_t dataRecvSize = 0;
	wsaFlags = 0;
	BOOL resultXWsaGetOverlappedResult = FALSE;
	if (use_xlive_networking) {
		resultXWsaGetOverlappedResult = XWSAGetOverlappedResult(socketMain, &wsaOverlapped, &dataRecvSize, FALSE, &wsaFlags);
	}
	else {
		resultXWsaGetOverlappedResult = WSAGetOverlappedResult(socketMain, &wsaOverlapped, (DWORD*)&dataRecvSize, FALSE, (DWORD*)&wsaFlags);
	}
	if (!resultXWsaGetOverlappedResult) {
		int32_t errorXWsaGetOverlappedResult = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorXWsaGetOverlappedResult, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSAGetOverlappedResult failed."
			, __func__
		);
		resultSuccess = false;
	}
	
	BOOL resultXWsaCloseEvent = FALSE;
	if (use_xlive_networking) {
		resultXWsaCloseEvent = XWSACloseEvent(wsaEvent);
	}
	else {
		resultXWsaCloseEvent = WSACloseEvent(wsaEvent);
	}
	if (!resultXWsaCloseEvent) {
		int32_t errorWsaCloseEvent = WSAGetLastError();
		XLLN_PRINT_LOG_ECODE(errorWsaCloseEvent, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XWSACloseEvent failed."
			, __func__
		);
		resultSuccess = false;
	}
	wsaEvent = 0;
	
	return resultSuccess;
}
