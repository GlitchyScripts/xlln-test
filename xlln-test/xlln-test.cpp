#include <WinSock2.h>
#include <windows.h>
#include <delayimp.h>
#include "xlln-test.hpp"
#include <stdio.h>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include <sys/timeb.h>
#include "utils/utils.hpp"
#include "xlive/xfuncs.hpp"
#include "xlln-test-network.hpp"

static const char xlive_dll[] = "XLIVE.dll";
HMODULE xlln_hmod_xlive = 0;
bool xlln_is_XLLNDebugLogF_available = true;

static bool RunXllnLogin(uint32_t user_index, bool live_enabled, bool online_enabled)
{
	if (!XLIVE_IMPORT_EXISTS("XLLNLogin")) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLLNLogin was not loaded."
			, __func__
		);
		return false;
	}
	
	if (user_index > 100) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s user_index (0x%08x) too large."
			, __func__
			, user_index
		);
		return false;
	}
	
	char playerUsernameIdx[] = "xlln-test-pi00";
	playerUsernameIdx[sizeof(playerUsernameIdx) - 3] = '0' + (user_index / 10);
	playerUsernameIdx[sizeof(playerUsernameIdx) - 2] = '0' + (user_index % 10);
	uint32_t resultLogin = XLLNLogin(
		user_index
		, ((live_enabled ? 0x01 : 0) + (online_enabled ? 0 : 0x0100))
		, 0
		, playerUsernameIdx
	);
	
	if (resultLogin != ERROR_SUCCESS) {
		XLLN_PRINT_LOG_ECODE(resultLogin, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLLNLogin failed."
			, __func__
		);
		return false;
	}
	
	return true;
}

static bool RunXllnLogout(uint32_t user_index)
{
	if (!XLIVE_IMPORT_EXISTS("XLLNLogout")) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLLNLogout was not loaded."
			, __func__
		);
		return false;
	}
	
	uint32_t resultLogout = XLLNLogout(user_index);
	if (resultLogout != ERROR_SUCCESS) {
		XLLN_PRINT_LOG_ECODE(resultLogout, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLLNLogout failed."
			, __func__
		);
		return false;
	}
	
	return true;
}

static bool RunSignin()
{
	wchar_t playerUsernameIdx0[] = L"xlln-test-pi0";
	wchar_t playerPasswordIdx0[] = L"";
	HRESULT resultXLiveSignin = XLiveSignin(playerUsernameIdx0, playerPasswordIdx0, 0, 0);
	if (resultXLiveSignin != S_OK) {
		XLLN_PRINT_LOG_ECODE(resultXLiveSignin, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLiveSignin failed."
			, __func__
		);
		return false;
	}
	
	return true;
}

static bool RunSignout()
{
	// Signout only player index 0, the argument is an overlapped pointer not an index.
	HRESULT resultXLiveSignout = XLiveSignout(0);
	if (resultXLiveSignout != S_OK) {
		XLLN_PRINT_LOG_ECODE(resultXLiveSignout, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XLiveSignout failed."
			, __func__
		);
		return false;
	}
	
	return true;
}

static bool RunTests()
{
	bool resultRunSignin = RunSignin();
	if (!resultRunSignin) {
		return false;
	}
	
	wchar_t userAction[100];
	userAction[0] = 0;
	XOVERLAPPED overlapped;
	memset(&overlapped, 0, sizeof(overlapped));
	overlapped.InternalLow = ERROR_IO_PENDING;
	uint32_t resultXShowKeyboardUI = XShowKeyboardUI(
		0, 0
		, L"done"
		, L"XLLN-Test"
		, L"Submit to exit."
		, userAction, sizeof(userAction)/sizeof(userAction[0])
		, &overlapped
	);
	if (resultXShowKeyboardUI != ERROR_IO_PENDING) {
		XLLN_PRINT_LOG_ECODE(resultXShowKeyboardUI, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XShowKeyboardUI failed."
			, __func__
		);
		bool resultRunSignout = RunSignout();
		return false;
	}
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XShowKeyboardUI succeeded."
		, __func__
	);
	
	while (overlapped.InternalLow == ERROR_IO_PENDING) {
		// Sleep for half a second.
		Sleep(500);
	}
	
	{
		bool resultRunSignout = RunSignout();
		
		return resultRunSignout;
	}
}

static bool RunTestsXUserProfileSettings()
{
	bool resultRunSignin = RunSignin();
	if (!resultRunSignin) {
		return false;
	}
	
	uint32_t settingIds[] = {XPROFILE_TITLE_SPECIFIC1, XPROFILE_TITLE_SPECIFIC2, XPROFILE_TITLE_SPECIFIC3};
	const size_t settingIdsCount = sizeof(settingIds)/sizeof(settingIds[0]);
	// Add more than max data to test truncation.
	const uint32_t profileSettingDataBlockSize = XPROFILE_SETTING_MAX_SIZE + 24;
	const bool emptyLastSetting = true;
	
	{
		XUSER_PROFILE_SETTING profileSettingsToWrite[settingIdsCount];
		uint8_t profileSettingDataBlocks[settingIdsCount][profileSettingDataBlockSize];
		
		for (size_t iSetting = 0; iSetting < settingIdsCount; iSetting++) {
			profileSettingsToWrite[iSetting].dwSettingId = settingIds[iSetting];
			profileSettingsToWrite[iSetting].user.dwUserIndex = 0;
			profileSettingsToWrite[iSetting].user.xuid = INVALID_XUID;
			profileSettingsToWrite[iSetting].source = XSOURCE_TITLE;
			profileSettingsToWrite[iSetting].data.type = XUSER_DATA_TYPE_BINARY;
			profileSettingsToWrite[iSetting].data.binary.cbData = profileSettingDataBlockSize;
			profileSettingsToWrite[iSetting].data.binary.pbData = profileSettingDataBlocks[iSetting];
			for (uint32_t iData = 0; iData < profileSettingsToWrite[iSetting].data.binary.cbData; iData++) {
				profileSettingsToWrite[iSetting].data.binary.pbData[iData] = (iData % 0xFF);
			}
		}
		
		if (emptyLastSetting) {
			profileSettingsToWrite[settingIdsCount-1].data.binary.cbData = 0;
		}
		
		uint32_t resultXUserWriteProfileSettings = XUserWriteProfileSettings(0, settingIdsCount, profileSettingsToWrite, 0);
		if (resultXUserWriteProfileSettings) {
			XLLN_PRINT_LOG_ECODE(resultXUserWriteProfileSettings, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XUserWriteProfileSettings failed."
				, __func__
			);
			bool resultRunSignout = RunSignout();
			return false;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s XUserWriteProfileSettings succeeded."
			, __func__
		);
	}
	
	{
		size_t resultSize = 0;
		
		uint32_t resultXUserReadProfileSettings = XUserReadProfileSettings(0, 0, settingIdsCount, settingIds, &resultSize, 0, 0);
		if (resultXUserReadProfileSettings != ERROR_INSUFFICIENT_BUFFER) {
			XLLN_PRINT_LOG_ECODE(resultXUserReadProfileSettings, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XUserReadProfileSettings failed with unexpected error."
				, __func__
			);
			bool resultRunSignout = RunSignout();
			return false;
		}
		
		XUSER_READ_PROFILE_SETTING_RESULT* profileSettingsRead = (XUSER_READ_PROFILE_SETTING_RESULT*)malloc(resultSize);
		
		resultXUserReadProfileSettings = XUserReadProfileSettings(0, 0, settingIdsCount, settingIds, &resultSize, profileSettingsRead, 0);
		if (resultXUserReadProfileSettings) {
			XLLN_PRINT_LOG_ECODE(resultXUserReadProfileSettings, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XUserReadProfileSettings failed."
				, __func__
			);
			free(profileSettingsRead);
			profileSettingsRead = 0;
			bool resultRunSignout = RunSignout();
			return false;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s XUserReadProfileSettings succeeded."
			, __func__
		);
		
		bool dataIsCorrect = true;
		
		dataIsCorrect &= (profileSettingsRead->dwSettingsLen == settingIdsCount);
		
		for (size_t iSetting = 0; iSetting < settingIdsCount; iSetting++) {
			dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].dwSettingId == settingIds[iSetting]);
			profileSettingsRead->pSettings[iSetting].user.dwUserIndex;
			profileSettingsRead->pSettings[iSetting].user.xuid;
			dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].source == XSOURCE_TITLE);
			dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].data.type == XUSER_DATA_TYPE_BINARY);
			if (!dataIsCorrect) {
				break;
			}
			if (emptyLastSetting && iSetting == settingIdsCount-1) {
				dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].data.binary.cbData == 0);
			}
			else {
				dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].data.binary.cbData == (profileSettingDataBlockSize > XPROFILE_SETTING_MAX_SIZE ? XPROFILE_SETTING_MAX_SIZE : profileSettingDataBlockSize));
			}
			if (!dataIsCorrect) {
				break;
			}
			for (uint32_t iData = 0; iData < profileSettingsRead->pSettings[iSetting].data.binary.cbData; iData++) {
				dataIsCorrect &= (profileSettingsRead->pSettings[iSetting].data.binary.pbData[iData] == (iData % 0xFF));
			}
			if (!dataIsCorrect) {
				break;
			}
		}
		
		if (!dataIsCorrect) {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XUserReadProfileSettings actually returned corrupted data."
				, __func__
			);
		}
		
		free(profileSettingsRead);
		profileSettingsRead = 0;
		
		if (!dataIsCorrect) {
			bool resultRunSignout = RunSignout();
			
			return false;
		}
	}
	
	{
		bool resultRunSignout = RunSignout();
		
		return resultRunSignout;
	}
}

static bool RunXHVEngine(IXHVEngine* xhvEngine, uint32_t user_local_count, uint32_t user_remote_count)
{
	if (user_local_count == 0) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s user_local_count must not be 0."
			, __func__
		);
		return false;
	}
	if (user_local_count <= user_remote_count) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s user_local_count (%u) <= user_remote_count (%u)."
			, __func__
			, user_local_count
			, user_remote_count
		);
		return false;
	}
	
	wchar_t userAction[100];
	userAction[0] = 0;
	XOVERLAPPED overlapped;
	memset(&overlapped, 0, sizeof(overlapped));
	overlapped.InternalLow = ERROR_IO_PENDING;
	uint32_t resultXShowKeyboardUI = XShowKeyboardUI(
		0, 0
		, L"done"
		, L"XHV Engine"
		, L"Submit to exit."
		, userAction, sizeof(userAction)/sizeof(userAction[0])
		, &overlapped
	);
	if (resultXShowKeyboardUI != ERROR_IO_PENDING) {
		XLLN_PRINT_LOG_ECODE(resultXShowKeyboardUI, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s XShowKeyboardUI failed."
			, __func__
		);
		return false;
	}
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XShowKeyboardUI succeeded."
		, __func__
	);
	
	// -- Call Summary --
	// RegisterLocalTalker
	// StartLocalProcessingModes
	// loop:
	//   IsHeadsetPresent
	//   IsLocalTalking
	//   GetLocalChatData
	// StopLocalProcessingModes
	// UnregisterLocalTalker
	
	#define XHV_LOCK_UNLOCK(type) {\
		HRESULT resultLock = xhvEngine->Lock(type);\
		if (resultLock != S_OK) {\
			XLLN_PRINT_LOG_ECODE(resultLock, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR\
				, "%s XHVEngine Lock(%u) failed."\
				, __func__\
				, type\
			);\
			return false;\
		}\
	}
	#define XHV_LOCK() XHV_LOCK_UNLOCK(XHV_LOCK_TYPE_LOCK)
	#define XHV_UNLOCK() XHV_LOCK_UNLOCK(XHV_LOCK_TYPE_UNLOCK)
	
	for (uint32_t iUser = 0; iUser < user_local_count; iUser++) {
		{
			XHV_LOCK();
			HRESULT resultRegisterLocalTalker = xhvEngine->RegisterLocalTalker(iUser);
			XHV_UNLOCK();
			if (resultRegisterLocalTalker) {
				XLLN_PRINT_LOG_ECODE(resultRegisterLocalTalker, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine RegisterLocalTalker failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine RegisterLocalTalker succeeded."
				, __func__
			);
		}
		
		{
			XHV_PROCESSING_MODE processingModes[] = {XHV_VOICECHAT_MODE, XHV_LOOPBACK_MODE};
			size_t processingModesCount = sizeof(processingModes)/sizeof(processingModes[0]);
			
			if (user_local_count != 1) {
				processingModesCount = 1;
			}
			
			XHV_LOCK();
			HRESULT resultStartLocalProcessingModes = xhvEngine->StartLocalProcessingModes(iUser, processingModes, processingModesCount);
			XHV_UNLOCK();
			if (resultStartLocalProcessingModes) {
				XLLN_PRINT_LOG_ECODE(resultStartLocalProcessingModes, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine StartLocalProcessingModes failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine StartLocalProcessingModes succeeded."
				, __func__
			);
		}
	}
	
	for (uint32_t iUser = (user_local_count - user_remote_count); iUser < user_local_count; iUser++) {
		XUID userRemoteXuid = 0;
		uint32_t resultXUserGetXUID = XUserGetXUID(iUser, &userRemoteXuid);
		if (resultXUserGetXUID) {
			XLLN_PRINT_LOG_ECODE(resultXUserGetXUID, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s Register XUserGetXUID failed."
				, __func__
			);
			return false;
		}
		
		{
			XHV_LOCK();
			HRESULT resultRegisterRemoteTalker = xhvEngine->RegisterRemoteTalker(userRemoteXuid, 0, 0, 0);
			XHV_UNLOCK();
			if (resultRegisterRemoteTalker) {
				XLLN_PRINT_LOG_ECODE(resultRegisterRemoteTalker, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine RegisterRemoteTalker failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine RegisterRemoteTalker succeeded."
				, __func__
			);
		}
		
		{
			XHV_LOCK();
			HRESULT resultSetPlaybackPriority = xhvEngine->SetPlaybackPriority(userRemoteXuid, iUser, XHV_PLAYBACK_PRIORITY_NEVER);
			XHV_UNLOCK();
			if (resultSetPlaybackPriority) {
				XLLN_PRINT_LOG_ECODE(resultSetPlaybackPriority, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine SetPlaybackPriority failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine SetPlaybackPriority succeeded."
				, __func__
			);
		}
		
		{
			XHV_PROCESSING_MODE processingModes[] = {XHV_VOICECHAT_MODE};
			size_t processingModesCount = sizeof(processingModes)/sizeof(processingModes[0]);
			
			XHV_LOCK();
			HRESULT resultStartRemoteProcessingModes = xhvEngine->StartRemoteProcessingModes(userRemoteXuid, processingModes, processingModesCount);
			XHV_UNLOCK();
			if (resultStartRemoteProcessingModes) {
				XLLN_PRINT_LOG_ECODE(resultStartRemoteProcessingModes, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine StartRemoteProcessingModes failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine StartRemoteProcessingModes succeeded."
				, __func__
			);
		}
	}
	
	{
		uint32_t localTalkerCount = user_local_count;
		bool* isHeadsetPresent = new bool[localTalkerCount];
		memset(isHeadsetPresent, false, sizeof(isHeadsetPresent[0]) * localTalkerCount);
		bool* isTalking = new bool[localTalkerCount];
		memset(isTalking, true, sizeof(isTalking[0]) * localTalkerCount);
		
		__timeb64* nextVoiceDataBandwidthStatistic = new __timeb64[localTalkerCount];
		for (uint32_t iUser = 0; iUser < localTalkerCount; iUser++) {
			_ftime64_s(&nextVoiceDataBandwidthStatistic[iUser]);
			nextVoiceDataBandwidthStatistic[iUser].time++;
		}
		
		size_t* lastPacketCount = new size_t[localTalkerCount];
		memset(lastPacketCount, 0, sizeof(lastPacketCount[0]) * localTalkerCount);
		
		const size_t voiceDataPacketCountsLength = 4;
		size_t(* voiceDataPacketCounts)[4] = new size_t[localTalkerCount][voiceDataPacketCountsLength];
		memset(voiceDataPacketCounts, 0, sizeof(voiceDataPacketCounts[0]) * localTalkerCount);
		
		uint8_t voiceData[XHV_MAX_VOICECHAT_PACKETS * XHV_VOICECHAT_MODE_PACKET_SIZE_OLD];
		
		std::map<XUID, std::vector<uint8_t*>> remoteSubmissions;
		const uint32_t packetLossEvery = 20;
		uint32_t packetLossCounter = 0;
		__timeb64 packetConsumeDelay = {0, 0, 0, 0};
		
		while (overlapped.InternalLow == ERROR_IO_PENDING) {
			Sleep(5);
			
			__timeb64 currentTime;
			_ftime64_s(&currentTime);
			
			if (IsTimeLessThan(packetConsumeDelay, currentTime)) {
				packetConsumeDelay = currentTime;
				TimeAddMillis(packetConsumeDelay, 10);
				
				for (auto &remoteSubmission : remoteSubmissions) {
					auto &voiceDataSubmittions = remoteSubmission.second;
					
					while (1) {
						auto itrVoiceDataSubmittion = voiceDataSubmittions.begin();
						if (itrVoiceDataSubmittion == voiceDataSubmittions.end()) {
							break;
						}
						uint8_t* voicePacket = *itrVoiceDataSubmittion;
						voiceDataSubmittions.erase(itrVoiceDataSubmittion);
						
						HRESULT resultSubmitIncomingChatData = S_OK;
						
						if (packetLossEvery && ++packetLossCounter == packetLossEvery) {
							packetLossCounter = 0;
							XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_DEBUG
								, "%s XHVEngine SubmitIncomingChatData simulated packet loss."
								, __func__
							);
						}
						else {
							size_t voicePacketSize = XHV_VOICECHAT_MODE_PACKET_SIZE_OLD;
							resultSubmitIncomingChatData = xhvEngine->SubmitIncomingChatData(remoteSubmission.first, voicePacket, &voicePacketSize);
						}
						
						delete[] voicePacket;
						voicePacket = 0;
						
						if (resultSubmitIncomingChatData != S_OK) {
							XLLN_PRINT_LOG_ECODE(resultSubmitIncomingChatData, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
								, "%s XHVEngine SubmitIncomingChatData(0x%016I64x) failed."
								, __func__
								, remoteSubmission.first
							);
							break;
						}
					}
				}
			}
			
			{
				XHV_LOCK();
				
				for (uint32_t iUser = 0; iUser < localTalkerCount; iUser++) {
					BOOL resultIsHeadsetPresent = xhvEngine->IsHeadsetPresent(iUser);
					//XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_DEBUG
					//	, "%s XHVEngine IsHeadsetPresent(%u) == %s."
					//	, __func__
					//	, iUser
					//	, ((!!resultIsHeadsetPresent) ? "true" : "false")
					//);
					
					BOOL resultIsLocalTalking = xhvEngine->IsLocalTalking(iUser);
					//XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_DEBUG
					//	, "%s XHVEngine IsLocalTalking(%u) == %s."
					//	, __func__
					//	, iUser
					//	, ((!!resultIsLocalTalking) ? "true" : "false")
					//);
					
					if (isHeadsetPresent[iUser] != (!!resultIsHeadsetPresent) || isTalking[iUser] != (!!resultIsLocalTalking)) {
						isHeadsetPresent[iUser] = (!!resultIsHeadsetPresent);
						isTalking[iUser] = (!!resultIsLocalTalking);
						XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
							, "%s XHVEngine IsHeadsetPresent(%u) == %s, IsLocalTalking(%u) == %s."
							, __func__
							, iUser
							, (isHeadsetPresent[iUser] ? "true " : "false")
							, iUser
							, (isTalking[iUser] ? "true " : "false")
						);
					}
					
					size_t voiceDataPacketTotalCount = 0;
					size_t voiceDataPacketCount = 0;
					do {
						voiceDataPacketCount = 0;
						
						size_t voiceDataSize = sizeof(voiceData);
						HRESULT resultGetLocalChatData = xhvEngine->GetLocalChatData(iUser, voiceData, &voiceDataSize, &voiceDataPacketCount);
						if (resultGetLocalChatData != S_OK && resultGetLocalChatData != E_PENDING) {
							XLLN_PRINT_LOG_ECODE(resultGetLocalChatData, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
								, "%s XHVEngine GetLocalChatData(%u) failed."
								, __func__
								, iUser
							);
							break;
						}
						
						voiceDataPacketTotalCount += voiceDataPacketCount;
						
						if (iUser >= (user_local_count - user_remote_count) && voiceDataPacketCount) {
							XUID userRemoteXuid = 0;
							uint32_t resultXUserGetXUID = XUserGetXUID(iUser, &userRemoteXuid);
							if (resultXUserGetXUID) {
								XLLN_PRINT_LOG_ECODE(resultXUserGetXUID, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
									, "%s Submit XUserGetXUID failed."
									, __func__
								);
								break;
							}
							
							auto &voiceDataSubmittions = remoteSubmissions[userRemoteXuid];
							for (size_t iPacket = 0; iPacket < voiceDataPacketCount; iPacket++) {
								uint8_t* voicePacket = new uint8_t[XHV_VOICECHAT_MODE_PACKET_SIZE_OLD];
								memcpy_s(
									voicePacket
									, XHV_VOICECHAT_MODE_PACKET_SIZE_OLD
									, &voiceData[iPacket * XHV_VOICECHAT_MODE_PACKET_SIZE_OLD]
									, XHV_VOICECHAT_MODE_PACKET_SIZE_OLD
								);
								voiceDataSubmittions.push_back(voicePacket);
							}
						}
					}
					while (voiceDataPacketCount);
					
					voiceDataPacketCounts[iUser][voiceDataPacketCountsLength - 1] += voiceDataPacketTotalCount;
					
					__timeb64 currentTime;
					_ftime64_s(&currentTime);
					if (nextVoiceDataBandwidthStatistic[iUser].time < currentTime.time) {
						nextVoiceDataBandwidthStatistic[iUser].time++;
						
						size_t voiceDataCombinedPacketCount = voiceDataPacketCounts[iUser][0];
						for (size_t iCount = 1; iCount < voiceDataPacketCountsLength; iCount++) {
							voiceDataCombinedPacketCount += (voiceDataPacketCounts[iUser][iCount - 1] = voiceDataPacketCounts[iUser][iCount]);
						}
						
						voiceDataPacketCounts[iUser][voiceDataPacketCountsLength - 1] = 0;
						
						size_t voiceDataCombinedSizeCount = (voiceDataCombinedPacketCount * (XHV_VOICECHAT_MODE_PACKET_SIZE_OLD - sizeof(XHV_CODEC_HEADER)));
						size_t reportPacketCount = voiceDataCombinedPacketCount / voiceDataPacketCountsLength;
						size_t reportSizeCount = voiceDataCombinedSizeCount / voiceDataPacketCountsLength;
						
						if (reportPacketCount != lastPacketCount[iUser]) {
							lastPacketCount[iUser] = reportPacketCount;
							
							XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
								, "%s XHVEngine STATISTIC User=%u, packets/sec=%zu, bytes/sec=%zu."
								, __func__
								, iUser
								, reportPacketCount
								, reportSizeCount
							);
						}
					}
				}
				
				XHV_UNLOCK();
			}
		}
		
		for (auto &remoteSubmission : remoteSubmissions) {
			auto &voiceDataSubmittions = remoteSubmission.second;
			
			while (1) {
				auto itrVoiceDataSubmittion = voiceDataSubmittions.begin();
				if (itrVoiceDataSubmittion == voiceDataSubmittions.end()) {
					break;
				}
				uint8_t* voicePacket = *itrVoiceDataSubmittion;
				voiceDataSubmittions.erase(itrVoiceDataSubmittion);
				
				delete[] voicePacket;
				voicePacket = 0;
			}
		}
		remoteSubmissions.clear();
		
		delete[] isHeadsetPresent;
		isHeadsetPresent = 0;
		delete[] isTalking;
		isTalking = 0;
		delete[] nextVoiceDataBandwidthStatistic;
		nextVoiceDataBandwidthStatistic = 0;
		delete[] lastPacketCount;
		lastPacketCount = 0;
		delete[] voiceDataPacketCounts;
		voiceDataPacketCounts = 0;
	}
	
	// Check xhvEngine->Release() without having stopped processing and unregistering users beforehand.
	//return false;
	
	for (uint32_t iUser = (user_local_count - user_remote_count); iUser < user_local_count; iUser++) {
		XUID userRemoteXuid = 0;
		uint32_t resultXUserGetXUID = XUserGetXUID(iUser, &userRemoteXuid);
		if (resultXUserGetXUID) {
			XLLN_PRINT_LOG_ECODE(resultXUserGetXUID, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s Unregister XUserGetXUID failed."
				, __func__
			);
			return false;
		}
		
		{
			XHV_PROCESSING_MODE processingModes[] = {XHV_VOICECHAT_MODE, XHV_LOOPBACK_MODE};
			size_t processingModesCount = sizeof(processingModes)/sizeof(processingModes[0]);
			
			if (user_local_count != 1) {
				processingModesCount = 1;
			}
			
			XHV_LOCK();
			HRESULT resultStopRemoteProcessingModes = xhvEngine->StopRemoteProcessingModes(userRemoteXuid, processingModes, processingModesCount);
			XHV_UNLOCK();
			if (resultStopRemoteProcessingModes) {
				XLLN_PRINT_LOG_ECODE(resultStopRemoteProcessingModes, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine StopRemoteProcessingModes failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine StopRemoteProcessingModes succeeded."
				, __func__
			);
		}
		
		{
			XHV_LOCK();
			HRESULT resultUnregisterRemoteTalker = xhvEngine->UnregisterRemoteTalker(userRemoteXuid);
			XHV_UNLOCK();
			if (resultUnregisterRemoteTalker) {
				XLLN_PRINT_LOG_ECODE(resultUnregisterRemoteTalker, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine UnregisterRemoteTalker failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine UnregisterRemoteTalker succeeded."
				, __func__
			);
		}
	}
	
	for (uint32_t iUser = 0; iUser < user_local_count; iUser++) {
		{
			XHV_PROCESSING_MODE processingModes[] = {XHV_VOICECHAT_MODE, XHV_LOOPBACK_MODE};
			size_t processingModesCount = sizeof(processingModes)/sizeof(processingModes[0]);
			
			if (user_local_count != 1) {
				processingModesCount = 1;
			}
			
			XHV_LOCK();
			HRESULT resultStopLocalProcessingModes = xhvEngine->StopLocalProcessingModes(iUser, processingModes, processingModesCount);
			XHV_UNLOCK();
			if (resultStopLocalProcessingModes) {
				XLLN_PRINT_LOG_ECODE(resultStopLocalProcessingModes, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine StopLocalProcessingModes failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine StopLocalProcessingModes succeeded."
				, __func__
			);
		}
		
		{
			XHV_LOCK();
			HRESULT resultUnregisterLocalTalker = xhvEngine->UnregisterLocalTalker(iUser);
			XHV_UNLOCK();
			if (resultUnregisterLocalTalker) {
				XLLN_PRINT_LOG_ECODE(resultUnregisterLocalTalker, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XHVEngine UnregisterLocalTalker failed."
					, __func__
				);
				return false;
			}
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine UnregisterLocalTalker succeeded."
				, __func__
			);
		}
	}
	
	#undef XHV_UNLOCK
	#undef XHV_LOCK
	#undef XHV_LOCK_UNLOCK
	
	return true;
}

static bool RunTestsXHVEngine(uint32_t user_local_count, uint32_t user_remote_count)
{
	if (user_local_count == 0) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s user_local_count must not be 0."
			, __func__
		);
		return false;
	}
	if (user_local_count <= user_remote_count) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
			, "%s user_local_count (%u) <= user_remote_count (%u)."
			, __func__
			, user_local_count
			, user_remote_count
		);
		return false;
	}
	
	for (uint32_t iUser = 0; iUser < user_local_count; iUser++) {
		bool resultRunSignin = RunXllnLogin(iUser, true, true);
		if (!resultRunSignin) {
			return false;
		}
	}
	
	{
		XHV_INIT_PARAMS xhvInitParams;
		xhvInitParams.bCustomVADProvided = false;
		xhvInitParams.pfnMicrophoneRawDataReady = 0;
		// TODO cannot be null.
		xhvInitParams.hwndFocus = 0;
		xhvInitParams.hwndFocus = (HWND)CreateMutexA(NULL, false, NULL);
		xhvInitParams.bRelaxPrivileges = true;
		xhvInitParams.dwMaxLocalTalkers = user_local_count;
		xhvInitParams.dwMaxRemoteTalkers = XHV_MAX_REMOTE_TALKERS;
		XHV_PROCESSING_MODE processingModesLocal[] = {XHV_LOOPBACK_MODE, XHV_VOICECHAT_MODE};
		xhvInitParams.dwNumLocalTalkerEnabledModes = sizeof(processingModesLocal)/sizeof(processingModesLocal[0]);
		xhvInitParams.localTalkerEnabledModes = processingModesLocal;
		XHV_PROCESSING_MODE processingModesRemote[] = {XHV_LOOPBACK_MODE, XHV_VOICECHAT_MODE};
		xhvInitParams.dwNumRemoteTalkerEnabledModes = sizeof(processingModesRemote)/sizeof(processingModesRemote[0]);
		xhvInitParams.remoteTalkerEnabledModes = processingModesRemote;
		
		IXHVEngine* xhvEngine;
		
		INT resultXHVCreateEngine = XHVCreateEngine(&xhvInitParams, 0, &xhvEngine);
		if (resultXHVCreateEngine) {
			XLLN_PRINT_LOG_ECODE(resultXHVCreateEngine, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XHVCreateEngine failed."
				, __func__
			);
			for (uint32_t iUser = 0; iUser < user_local_count; iUser++) {
				bool resultRunSignout = RunXllnLogout(iUser);
			}
			return false;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s XHVCreateEngine succeeded."
			, __func__
		);
		
		RunXHVEngine(xhvEngine, user_local_count, user_remote_count);
		
		LONG resultRelease = xhvEngine->Release();
		if (resultRelease != 0) {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XHVEngine failed to perform final Release() with result (0x%08x)."
				, __func__
				, resultRelease
			);
		}
		else {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s XHVEngine final Release() succeeded."
				, __func__
			);
		}
	}
	
	{
		bool resultCompleteSignoutSuccess = true;
		
		for (uint32_t iUser = 0; iUser < user_local_count; iUser++) {
			resultCompleteSignoutSuccess &= RunXllnLogout(iUser);
		}
		
		return resultCompleteSignoutSuccess;
	}
}

static bool RunTestsNetworkHelper()
{
	bool useXliveNetworking = true;
	
	{
		const wchar_t* userActions[] = {L"XLive", L"WinSock"};
		XOVERLAPPED overlapped;
		memset(&overlapped, 0, sizeof(overlapped));
		overlapped.InternalLow = ERROR_IO_PENDING;
		MESSAGEBOX_RESULT resultMsgBox;
		uint32_t resultXShowMessageBoxUI = XShowMessageBoxUI(
			0
			, L"XLLN Test"
			, L"Choose networking API:"
			, sizeof(userActions)/sizeof(userActions[0])
			, userActions
			, 0
			, 0
			, &resultMsgBox
			, &overlapped
		);
		if (resultXShowMessageBoxUI != ERROR_IO_PENDING) {
			XLLN_PRINT_LOG_ECODE(resultXShowMessageBoxUI, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
				, "%s XShowMessageBoxUI failed."
				, __func__
			);
			return false;
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s Asking for networking API."
			, __func__
		);
		
		while (overlapped.InternalLow == ERROR_IO_PENDING) {
			Sleep(100);
		}
		
		size_t extendedError = XGetOverlappedExtendedError(&overlapped);
		if (extendedError == ERROR_CANCELLED) {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s User cancelled."
				, __func__
			);
			return false;
		}
		
		switch (resultMsgBox.dwButtonPressed) {
			case 0: {
				useXliveNetworking = true;
				break;
			}
			case 1: {
				useXliveNetworking = false;
				break;
			}
			default: {
				XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_ERROR
					, "%s XShowMessageBoxUI unknown button pressed (0x%x)."
					, __func__
					, resultMsgBox.dwButtonPressed
				);
				return false;
			}
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
			, "%s User chose %ls."
			, __func__
			, userActions[resultMsgBox.dwButtonPressed]
		);
	}
	
	if (!useXliveNetworking) {
		// Request version 2.0.
		WORD wVersionRequested = MAKEWORD(2, 0);
		WSADATA wsaData;
		uint32_t resultWSAStartup = WSAStartup(wVersionRequested, &wsaData);
		if (resultWSAStartup) {
			XLLN_PRINT_LOG_ECODE(resultWSAStartup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
				, "%s WSAStartup failed."
				, __func__
			);
			return false;
		}
	}
	
	bool resultTestNetwork = XllnTestNetwork(useXliveNetworking);
	
	if (!useXliveNetworking) {
		INT resultWSACleanup = WSACleanup();
		if (resultWSACleanup) {
			XLLN_PRINT_LOG_ECODE(resultWSACleanup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
				, "%s XWSACleanup failed."
				, __func__
			);
			return false;
		}
	}
	
	return resultTestNetwork;
}

static bool RunTestsNetwork()
{
	{
		bool resultRunSignin = RunSignin();
		if (!resultRunSignin) {
			return false;
		}
	}
	
	bool resultTestNetwork = RunTestsNetworkHelper();
	
	{
		bool resultRunSignout = RunSignout();
		
		return resultRunSignout && resultTestNetwork;
	}
}

static bool RunXLiveUninitialize()
{
	XLiveUninitialize();
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XLiveUninitialize succeeded."
		, __func__
	);
	return true;
}

static bool RunXNetCleanup()
{
	INT resultXNetCleanup = XNetCleanup();
	if (resultXNetCleanup) {
		XLLN_PRINT_LOG_ECODE(resultXNetCleanup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XNetCleanup failed."
			, __func__
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XNetCleanup succeeded."
		, __func__
	);
	return true;
}

static bool RunXWSACleanup()
{
	INT resultXWSACleanup = XWSACleanup();
	if (resultXWSACleanup) {
		XLLN_PRINT_LOG_ECODE(resultXWSACleanup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XWSACleanup failed."
			, __func__
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XWSACleanup succeeded."
		, __func__
	);
	return true;
}

static bool RunXOnlineCleanup()
{
	uint32_t resultXOnlineCleanup = XOnlineCleanup();
	if (resultXOnlineCleanup) {
		XLLN_PRINT_LOG_ECODE(resultXOnlineCleanup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XOnlineCleanup failed."
			, __func__
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XOnlineCleanup succeeded."
		, __func__
	);
	return true;
}

static bool RunXLiveInitialize()
{
	XLIVE_INITIALIZE_INFO xliveInitInfo;
	memset(&xliveInitInfo, 0, sizeof(xliveInitInfo));
	xliveInitInfo.cbSize = sizeof(xliveInitInfo);
	
	HRESULT resultXliveInitialize = XLiveInitialize(&xliveInitInfo);
	if (FAILED(resultXliveInitialize)) {
		XLLN_PRINT_LOG_ECODE(resultXliveInitialize, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XLiveInitialize failed."
			, __func__
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XLiveInitialize succeeded."
		, __func__
	);
	
	XNetStartupParams xnetStartupParams;
	memset(&xnetStartupParams, 0, sizeof(xnetStartupParams));
	xnetStartupParams.cfgSizeOfStruct = sizeof(xnetStartupParams);
	xnetStartupParams.cfgFlags = 0;
	xnetStartupParams.cfgSockMaxDgramSockets = 8;
	xnetStartupParams.cfgSockMaxStreamSockets = 32;
	xnetStartupParams.cfgSockDefaultRecvBufsizeInK = 16;
	xnetStartupParams.cfgSockDefaultSendBufsizeInK = 16;
	xnetStartupParams.cfgKeyRegMax = 8;
	xnetStartupParams.cfgSecRegMax = 32;
	xnetStartupParams.cfgQosDataLimitDiv4 = 64;
	xnetStartupParams.cfgQosProbeTimeoutInSeconds = 2;
	xnetStartupParams.cfgQosProbeRetries = 3;
	xnetStartupParams.cfgQosSrvMaxSimultaneousResponses = 8;
	xnetStartupParams.cfgQosPairWaitTimeInSeconds = 2;
	
	INT resultXNetStartup = XNetStartup(&xnetStartupParams);
	if (resultXNetStartup) {
		XLLN_PRINT_LOG_ECODE(resultXNetStartup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XNetStartup failed."
			, __func__
		);
		bool resultRunXLiveUninitialize = RunXLiveUninitialize();
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XNetStartup succeeded."
		, __func__
	);
	
	// Request version 2.0.
	WORD wVersionRequested = MAKEWORD(2, 0);
	WSADATA wsaData;
	uint32_t resultXWSAStartup = XWSAStartup(wVersionRequested, &wsaData);
	if (resultXWSAStartup) {
		XLLN_PRINT_LOG_ECODE(resultXWSAStartup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XWSAStartup failed."
			, __func__
		);
		bool resultRunXNetCleanup = RunXNetCleanup();
		bool resultRunXLiveUninitialize = RunXLiveUninitialize();
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XWSAStartup succeeded. Requested version (%hhu.%hhu). Result support for version (%hhu.%hhu)."
		, __func__
		, (uint8_t)(wVersionRequested & 0xFF)
		, (uint8_t)((wVersionRequested >> 8) & 0xFF)
		, (uint8_t)(wsaData.wVersion & 0xFF)
		, (uint8_t)((wsaData.wVersion >> 8) & 0xFF)
	);
	
	uint32_t resultXOnlineStartup = XOnlineStartup();
	if (resultXOnlineStartup) {
		XLLN_PRINT_LOG_ECODE(resultXOnlineStartup, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s XOnlineStartup failed."
			, __func__
		);
		bool resultRunXWSACleanup = RunXWSACleanup();
		bool resultRunXNetCleanup = RunXNetCleanup();
		bool resultRunXLiveUninitialize = RunXLiveUninitialize();
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s XOnlineStartup succeeded."
		, __func__
	);
	
	//bool resultRunTests = true;
	//bool resultRunTests = RunTests();
	//bool resultRunTests = RunTestsXUserProfileSettings();
	//bool resultRunTests = RunTestsXHVEngine(2, 1);
	bool resultRunTests = RunTestsNetwork();
	
	{
		bool resultCompleteShutdownSuccess = true;
		resultCompleteShutdownSuccess &= resultRunTests;
		
		bool resultRunXOnlineCleanup = RunXOnlineCleanup();
		resultCompleteShutdownSuccess &= resultRunXOnlineCleanup;
		bool resultRunXWSACleanup = RunXWSACleanup();
		resultCompleteShutdownSuccess &= resultRunXWSACleanup;
		bool resultRunXNetCleanup = RunXNetCleanup();
		resultCompleteShutdownSuccess &= resultRunXNetCleanup;
		bool resultRunXLiveUninitialize = RunXLiveUninitialize();
		resultCompleteShutdownSuccess &= resultRunXLiveUninitialize;
		
		return resultCompleteShutdownSuccess;
	}
}

static bool UnloadXLive()
{
	if (xlln_hmod_xlive) {
		BOOL resultFreeLibrary = FreeLibrary(xlln_hmod_xlive);
		if (!resultFreeLibrary) {
			uint32_t errorFreeLibrary = GetLastError();
			XLLN_PRINT_LOG_ECODE(errorFreeLibrary, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
				, "%s FreeLibrary(0x%zx) failed."
				, __func__
				, (size_t)xlln_hmod_xlive
			);
		}
		else {
			XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
				, "%s FreeLibrary(0x%zx) succeeded."
				, __func__
				, (size_t)xlln_hmod_xlive
			);
		}
		xlln_hmod_xlive = 0;
	}
	
	BOOL resultUnloadDll = __FUnloadDelayLoadedDLL2(xlive_dll);
	if (!resultUnloadDll) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s __FUnloadDelayLoadedDLL2(\"%s\") failed."
			, __func__
			, xlive_dll
		);
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s __FUnloadDelayLoadedDLL2(\"%s\") succeeded."
		, __func__
		, xlive_dll
	);
	
	return true;
}

static int CheckDelayLoadException(int thrown_exception_value, struct _EXCEPTION_POINTERS* thrown_exception_info)
{
	if (thrown_exception_value == VcppException(ERROR_SEVERITY_ERROR, ERROR_MOD_NOT_FOUND)) {
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s \"%s\" ERROR_MOD_NOT_FOUND."
			, __func__
			, xlive_dll
		);
		return EXCEPTION_EXECUTE_HANDLER;
	}
	if (thrown_exception_value == VcppException(ERROR_SEVERITY_ERROR, ERROR_PROC_NOT_FOUND)) {
		if (thrown_exception_info && thrown_exception_info->ExceptionRecord && thrown_exception_info->ExceptionRecord->NumberParameters >= 1) {
			DelayLoadInfo* delayLoadInfo = (DelayLoadInfo*)thrown_exception_info->ExceptionRecord->ExceptionInformation[0];
			if (delayLoadInfo) {
				// Ignore any missing XLLN specific function exports (different from XLive function exports).
				if (delayLoadInfo->dlp.dwOrdinal >= 41140 && delayLoadInfo->dlp.dwOrdinal <= 41199) {
					XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_WARN
						, "%s \"%s\" ERROR_PROC_NOT_FOUND ordinal (%u)."
						, __func__
						, xlive_dll
						, delayLoadInfo->dlp.dwOrdinal
					);
					
					if (delayLoadInfo->dlp.dwOrdinal == 41144) {
						xlln_is_XLLNDebugLogF_available = false;
					}
					
					return EXCEPTION_CONTINUE_EXECUTION;
				}
				
				XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
					, "%s \"%s\" ERROR_PROC_NOT_FOUND ordinal (%u)."
					, __func__
					, xlive_dll
					, delayLoadInfo->dlp.dwOrdinal
				);
				return EXCEPTION_EXECUTE_HANDLER;
			}
		}
		
		XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s \"%s\" ERROR_PROC_NOT_FOUND unknown ordinal."
			, __func__
			, xlive_dll
		);
		return EXCEPTION_EXECUTE_HANDLER;
	}
	
	XLLN_PRINT_LOG_ECODE(thrown_exception_value, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
		, "%s \"%s\"."
		, __func__
		, xlive_dll
	);
	return EXCEPTION_CONTINUE_SEARCH;
}

static bool RunXLive()
{
	__try {
		HRESULT resultLoadAllImports = __HrLoadAllImportsForDll(xlive_dll);
		if (FAILED(resultLoadAllImports)) {
			XLLN_PRINT_LOG_ECODE(resultLoadAllImports, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
				, "%s __HrLoadAllImportsForDll(\"%s\") failed."
				, __func__
				, xlive_dll
			);
			return false;
		}
	}
	__except (CheckDelayLoadException(GetExceptionCode(), GetExceptionInformation())) {
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s __HrLoadAllImportsForDll(\"%s\") loaded."
		, __func__
		, xlive_dll
	);
	
	xlln_hmod_xlive = LoadLibraryA(xlive_dll);
	if (!xlln_hmod_xlive) {
		uint32_t errorLoadLibrary = GetLastError();
		XLLN_PRINT_LOG_ECODE(errorLoadLibrary, XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_FATAL
			, "%s LoadLibraryA(\"%s\") was not loaded."
			, __func__
			, xlive_dll
		);
		bool resultUnloadXlive = UnloadXLive();
		return false;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s LoadLibraryA(\"%s\") loaded at 0x%zx."
		, __func__
		, xlive_dll
		, (size_t)xlln_hmod_xlive
	);
	
	bool resultRunXLiveInitialize = RunXLiveInitialize();
	
	{
		bool resultUnloadXlive = UnloadXLive();
		
		return resultUnloadXlive;
	}
}

int main()
{
	int resultCode = EXIT_SUCCESS;
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s start."
		, __func__
	);
	
	bool successXlive = RunXLive();
	if (!successXlive) {
		resultCode = EXIT_FAILURE;
	}
	
	XLLN_PRINT_LOG(XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO
		, "%s end."
		, __func__
	);
	
	return resultCode;
}
